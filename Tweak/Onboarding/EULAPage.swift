import SwiftUI

struct EULAPage: View {
	@Environment(\.presentationMode) var presentationMode
	@State private var onboarding2 = false
	var body: some View {
		VStack {
			HStack{
				Button(action: {
				}) {
					Text("Accept")
						.foregroundColor(Color.white)
				}
				Spacer()
					.onTapGesture {
						presentationMode.wrappedValue.dismiss()
					}
				Text("License Agreement ")
					.bold()
					.onTapGesture {
						presentationMode.wrappedValue.dismiss()
					}
				Spacer()
					.onTapGesture {
						presentationMode.wrappedValue.dismiss()
					}
				Button(action: {
					self.onboarding2.toggle()
				}) {
					Text("Accept")
						.foregroundColor(Color("TweakOrange"))
				}.fullScreenCover(isPresented: $onboarding2, content: Onboarding2.init)
			}
			.background(Color(red: 248, green: 248, blue: 248, opacity: 1))
			.padding([.horizontal,.top])
			Divider()
			HStack{
				ScrollView{
					Text(
						"""
						Tweak 1.0
						Copyright (c) 2020 Tweak

						*** END USER LICENSE AGREEMENT ***

						IMPORTANT: PLEASE READ THIS LICENSE CAREFULLY BEFORE USING THIS SOFTWARE.

						1. LICENSE
						By downloading, and/or using Tweak 1.0("Software") containing this software, you agree that this End User User License Agreement(EULA) is a legally binding and valid contract and agree to be bound by it. You agree to abide by the intellectual property laws and all of the terms and conditions of this Agreement.

						Unless you have a different license agreement signed by Tweak.inc your use of Tweak 1.0 indicates your acceptance of this license agreement and warranty.

						Subject to the terms of this Agreement, Tweak grants to you a limited, non-exclusive, to use Tweak 1.0 in accordance with this Agreement and any other written agreement with Tweak. Tweak does not transfer the title of Tweak 1.0 to you; the license granted to you is not a sale. This agreement is a binding legal agreement between Tweak and the users of Tweak 1.0.

						If you do not agree to be bound by this agreement, remove Tweak 1.0 from your Device now.

						2. DISTRIBUTION
						Tweak 1.0 distributed free in app store. For information about redistribution of Tweak 1.0 contact Tweak.

						3. USER AGREEMENT

						3.1 Use
						Tweak 1.0 is completely Free for temporary until future updates.

						3.2 Use Restrictions
						You shall use Tweak 1.0 in compliance with all applicable laws and not for any unlawful purpose. Without limiting the foregoing, use, display or distribution of Tweak 1.0 together with material that is pornographic, racist, vulgar, obscene, defamatory, libelous, abusive, promoting hatred, discriminating or displaying prejudice based on religion, ethnic heritage, race, sexual orientation or age is strictly prohibited.

						3.3 Copyright Restriction
						This Software contains copyrighted material, trade secrets and other proprietary material. You shall not, and shall not attempt to, modify, reverse engineer, disassemble or decompile Tweak 1.0. Nor can you create any derivative works or other works that are based upon or derived from Tweak 1.0 in whole or in part.

						You will indemnify, hold harmless, and defend Tweak's Team against any and all claims, proceedings, demand and costs resulting from or in any way connected with your use of Tweak Software.

						In no event (including, without limitation, in the event of negligence) will Tweak , its employees, agents or distributors be liable for any consequential, incidental, indirect, special or punitive damages whatsoever (including, without limitation, damages for loss of profits, loss of use, business interruption, loss of information or data, or pecuniary loss), in connection with or arising out of or related to this Agreement, Tweak 1.0 or the use or inability to use Tweak 1.0 or the furnishing, performance or use of any other matters hereunder whether based upon contract, tort or any other theory including negligence.

						3.5 Warranties
						Except as expressly stated in writing, Tweak makes no representation or warranties in respect of this Software and expressly excludes all other warranties, expressed or implied, oral or written, including, without limitation, any implied warranties of merchantable quality or fitness for a particular purpose.

						3.6 Termination
						Any failure to comply with the terms and conditions of this Agreement will result in automatic and immediate termination of this license. Upon termination of this license granted herein for any reason, you agree to immediately cease use of Tweak 1.0 and destroy all copies of Tweak 1.0 supplied under this Agreement. The financial obligations incurred by you shall survive the expiration or termination of this license.

						3.7 User Generated Content
						User generated content on the App Store gets removed within 24h, if objectionable.

						4. DISCLAIMER OF WARRANTY
						THIS SOFTWARE AND THE ACCOMPANYING FILES ARE SOLD "AS IS" AND WITHOUT WARRANTIES AS TO PERFORMANCE OR MERCHANTABILITY OR ANY OTHER WARRANTIES WHETHER EXPRESSED OR IMPLIED. THIS DISCLAIMER CONCERNS ALL FILES GENERATED AND EDITED BY Tweak 1.0 AS WELL.

						5. CONSENT OF USE OF DATA
						You agree that Tweak.inc may collect and use information gathered in any manner as part of the product support services provided to you, if any, related to Tweak 1.0.Tweak.inc may also use this information to provide notices to you which may be of use or interest to you.
						""")				
				}
			}
			.background(Color(red: 245, green: 245, blue: 245, opacity: 1))
			.padding([.horizontal,.bottom])
		}
		.edgesIgnoringSafeArea(.all)
	}
}
