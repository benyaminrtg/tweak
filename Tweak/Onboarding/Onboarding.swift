import SwiftUI
import AVFoundation

struct Onboarding: View {
	@State var SplashScreenOffset:CGFloat = 0.0
	@State var featurePageOffset:CGFloat = UIScreen.screenHeight
	@State var EULA: Bool = true
	@State private var showEulaModal = false
	var body: some View {
		ZStack{
			Group{
				VStack {
					Image("onboardingBG")
						.onAppear{
							let path = Bundle.main.path(forResource: "quack", ofType:"mp3")!
							let url = URL(fileURLWithPath: path)
							
							do {
								print("\(url)")
								let quackSoundEffect: AVAudioPlayer? = try AVAudioPlayer(contentsOf: url)
								quackSoundEffect?.prepareToPlay()
								quackSoundEffect?.play()
								print("done")
							} catch {
								print("error")
							}
						}
					Spacer()
				}
				.animation(.easeInOut)
				.offset(x:0.0 , y: SplashScreenOffset)
				VStack{
					Spacer()
						.frame(height: UIScreen.screenHeight/5.4)
					HStack {
						Text("Quack!")
							.foregroundColor(.white)
							.font(.largeTitle)
							.fontWeight(.bold)
					}
					
					Spacer()
				}.animation(.easeInOut)
				.offset(x: 0.0 , y: SplashScreenOffset)
			}
			
			Group{
				VStack {
					Spacer()
						.frame(height: UIScreen.screenHeight/4.4)
					Group {
						HStack{
							Text("Welcome to")
								.font(.largeTitle)
								.fontWeight(.bold)
							Text("Tweak")
								.font(.largeTitle)
								.fontWeight(.bold)
								.foregroundColor(Color("TweakOrange"))
						}
						
						Spacer()
							.frame(height: UIScreen.screenHeight/13.7)
						HStack{
							Image(systemName: "quote.bubble.fill")
								.foregroundColor(Color("TweakOrange"))
								.frame(width: 75, height: 85)
								.font(.system(size: 38))
							VStack {
								HStack {
									Text("Explore top-players’ captions")
										.fontWeight(.semibold)
										.multilineTextAlignment(.leading)
									Spacer()
								}
								
								HStack {
									Text("Find caption inspirations from top business accounts based on your needs.")
										.frame(height:66)
										.multilineTextAlignment(.leading)
										.lineLimit(3)
									Spacer()
								}
							}
							Spacer()
						}
						.padding(.horizontal,35)
						Spacer()
							.frame(height:29)
						HStack {
							Image(systemName: "bookmark.fill")
								.foregroundColor(Color("TweakOrange"))
								.frame(width: 75, height: 85)
								.font(.system(size: 38))
							VStack {
								HStack {
									Text("Save your favorite captions")
										.fontWeight(.semibold)
										.multilineTextAlignment(.leading)
									Spacer()
								}
								HStack {
									Text("Find captions you like? Save them! Arrange in folders and easily find them anytime.")
										.frame(height:66)
										.multilineTextAlignment(.leading)
									Spacer()
								}
							}
							Spacer()
						}
						.padding(.horizontal,35)
						Spacer()
							.frame(height:29)
						HStack {
							Image(systemName: "square.and.pencil")
								.foregroundColor(Color("TweakOrange"))
								.frame(width: 75, height: 85)
								.font(.system(size: 38))
							VStack {
								HStack {
									Text("Tweak your captions")
										.fontWeight(.semibold)
										.multilineTextAlignment(.leading)
									Spacer()
								}
								HStack {
									Text("Tweak previously found caption inspirations to match your brand’s personality.")
										.frame(height:66)
										.multilineTextAlignment(.leading)
									Spacer()
								}
							}
							Spacer()
						}
						.padding(.horizontal,35)
					}
					Spacer()
						.frame(height: UIScreen.screenHeight/4.3)
					Group {
						VStack{
							HStack {
								Button(action: {
									self.showEulaModal.toggle()
								}) {
									Text("Let's Go!")
										.fontWeight(.semibold)
										.padding(.all)
										.padding(.horizontal,40)
								}
								.background(Color("TweakOrange"))
								.cornerRadius(25)
								.foregroundColor(.white)
								.sheet(isPresented: $showEulaModal) {
									EULAPage()
								}
							}
							.padding(.horizontal,30)
						}
					}
					Spacer()
						.frame(height: UIScreen.screenHeight/7.1)
				}
				.frame(maxWidth: UIScreen.screenWidth, maxHeight: UIScreen.screenHeight)
				.background(Color.white)
				.animation(.easeInOut)
				.offset(x: 0.0, y: featurePageOffset)
			}
		}
		.edgesIgnoringSafeArea(.all)
		.onAppear(perform: dismissSplash)
	}
	
	func dismissSplash(){
		DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
			SplashScreenOffset -= UIScreen.screenHeight
			featurePageOffset -= UIScreen.screenHeight
		}
	}
}
