import SwiftUI

struct Onboarding2: View {
	@Environment(\.presentationMode) var presentationMode
	@State private var showMainContent = false
	@State var searchText:String = ""
	@State var businessCategories:[String] = []
	var body: some View {
		VStack{
			Text("What is your business category?")
				.fontWeight(.semibold)
			Text("You can change this later")
				.font(.subheadline)
				.foregroundColor(.gray)
			Spacer()
				.frame(height: UIScreen.screenHeight/42.6)
			HStack {
				SearchBar(text: $searchText)
			}.padding(.horizontal)
			ScrollView{
				ForEach(businessCategories,id: \.self) { item in
					if(item.uppercased().contains(searchText.uppercased()) || searchText == ""){
						Button(action: {
							self.showMainContent.toggle()
							saveBusinessCategory(as: item)
						}) {
							Text(item.capitalized)
								.foregroundColor(.black)
							Spacer()
						}
						.frame(height: 44.0)
						.padding(.horizontal)
						.fullScreenCover(isPresented: $showMainContent, content: ContentView.init)
					}
				}
			}
			Spacer()
				.frame(height: UIScreen.screenHeight/59.7)
			HStack {
				Spacer()
				Button(action: {
					self.showMainContent.toggle()
					saveBusinessCategory(as: "None")
				}) {
					Text("Skip for now")
						.foregroundColor(.gray)
				}
				.fullScreenCover(isPresented: $showMainContent, content: ContentView.init)
				Spacer()
			}
			.padding()
			Spacer()
				.frame(height: UIScreen.screenHeight/20)
		}
		.onAppear{loadCategory()}
		.frame(maxWidth: UIScreen.screenWidth, maxHeight: UIScreen.screenHeight)
		.padding(.horizontal)
		.background(Color.white)
		.animation(.easeInOut)
	}
	
	func saveBusinessCategory(as category:String){
		let defaults = UserDefaults.standard
		defaults.set(true, forKey: "alreadyOnboarding")
		defaults.set(category, forKey: "businessCategory")
	}
	func loadCategory() {
		self.businessCategories.removeAll()
		guard let url = URL(string: "https://tweak.omkstyakobus.org/categorylist") else {
			print("Invalid URL")
			return
		}
		print(url)
		let request = URLRequest(url: url)
		URLSession.shared.dataTask(with: request) { data, response, error in
			if let data = data {
				let responseData = String(data: data, encoding: String.Encoding.utf8)!.replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: "").replacingOccurrences(of: "\"", with: "").components(separatedBy: ",")
				businessCategories = responseData
				print(businessCategories)
			}
		}.resume()
	}
}
