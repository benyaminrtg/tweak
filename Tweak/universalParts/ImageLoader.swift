//
//  ImageLoader.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import Foundation
import Combine
import SwiftUI

class ImageLoader: ObservableObject {
	var didChange = PassthroughSubject<Data, Never>()
	@Published var data = Data() {
		didSet {
			didChange.send(data)
		}
	}
	
	init(imageUrl:String) {
		guard let url = URL(string: imageUrl) else { return }
		let task = URLSession.shared.dataTask(with: url) { data, response, error in
			guard let data = data else { return }
			DispatchQueue.main.async {
				self.data = data
			}
		}
		task.resume()
	}
}
