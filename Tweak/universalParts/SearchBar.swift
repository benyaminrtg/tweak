//
//  SearchBar.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI

struct SearchBar: View {
	@Binding var text: String
	@State private var isEditing: Bool = false
	@State var isActive: Bool = false
	//@EnvironmentObject var userSettings: UserSettings
	//@EnvironmentObject var captionList: ListOfCaptions
	var body: some View {
		HStack {
			TextField("Search", text: $text)
				.padding(7)
				.padding(.horizontal, 25)
				.background(Color.init(red: 215/255, green: 215/255, blue: 215/255))
				.cornerRadius(8)
				.overlay(
					HStack {
						Image(systemName: "magnifyingglass")
							.foregroundColor(Color.init(red: 142/255, green: 142/255, blue: 147/255))
							.frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
							.padding(.leading, 8)
						
						if isEditing {
							Button(action: {
								self.text = ""
							}) {
								Image(systemName: "multiply.circle.fill")
									.foregroundColor(Color.init(red: 142/255, green: 142/255, blue: 147/255))
									.padding(.trailing, 8)
							}
						}
					}
				)
				.onTapGesture {
					self.isEditing = true
				}
			
			if isEditing {
				Button(action: {
					self.isEditing = false
					self.text = ""
					self.hideKeyboard()
				}) {
					Text("Cancel")
				}
				.padding(.trailing, 10)
			}
		}
	}
}

struct SearchBar_Previews: PreviewProvider {
    static var previews: some View {
		SearchBar(text: .constant("Hello"))
    }
}
