import SwiftUI

struct copiedToClipboard: View {
    var body: some View {
	
			Text("Copied to Clipboard")
				.font(.system(size: 14))
				.bold()
				.foregroundColor(.secondary)
				.padding()
				.padding(.horizontal)
				.background(Color.white)
				.cornerRadius(35)
				.shadow(color: Color.black.opacity(0.1),radius: 10, x:2, y:2)
    }
}
