import SwiftUI

struct captionUpdated: View {
	var body: some View {
		Text("Caption Saved")
			.foregroundColor(.secondary)
			.padding()
			.background(Color.white)
			.cornerRadius(35)
			.shadow(color: Color.black.opacity(0.1),radius: 10, x:2, y:2)
	}
}
