import SwiftUI
import CoreData

struct editCaptionView: View {
	@Environment(\.presentationMode) var presentationMode
	@Environment(\.managedObjectContext) var moc
	@State var caption:String
	@State var uuid:String
	@State var saved:CGFloat = -UIScreen.screenHeight
	
	@Environment(\.managedObjectContext) private var viewContext
	@FetchRequest(entity: Tweaked.entity(), sortDescriptors: []) var tweaked: FetchedResults<Tweaked>
	
	var body: some View {
		ZStack{
			TextEditor(text: $caption)
				.navigationBarBackButtonHidden(true)
				.navigationBarTitle("Modify Caption", displayMode: .inline)
				.navigationBarItems(leading:
										Button(action: {
											self.presentationMode.wrappedValue.dismiss()
										}) {
											HStack(spacing: 0){
												Image(systemName: "chevron.left")
													.foregroundColor(Color("TweakOrange"))
												Text(" Tweaked")
													.foregroundColor(Color("TweakOrange"))
													.fontWeight(.regular)
												Spacer()
											}
										},
									trailing:
										Button(action: {
											saveCaptionHistory(caption: caption, uuid: uuid)
											triggerCopiedNotif()
											
										}) {
											Text("Save")
												.foregroundColor(Color("TweakOrange"))
										}
				)
			VStack {
				Spacer()
					.frame(height:10)
				captionUpdated()
				Spacer()
			}
			.offset(y: saved)
			.animation(.easeInOut)
		}
	}
	
	func saveCaptionHistory(caption: String, uuid: String) {
		for item in tweaked {
			if item.id!.uuidString == uuid{
				item.tweakedBody = caption
			}
		}
		try? moc.save()
	}
	
	func triggerCopiedNotif(){
		DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
			saved += UIScreen.screenHeight
		}
		DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
			saved -= UIScreen.screenHeight
		}
	}
}
