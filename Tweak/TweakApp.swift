//
//  TweakApp.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI
import PartialSheet

@main
struct TweakApp: App {
    let persistenceController = PersistenceController.shared
	let defaults = UserDefaults.standard
	
	let sheetManager: PartialSheetManager = PartialSheetManager()
    var body: some Scene {
        WindowGroup {
			if defaults.bool(forKey: "alreadyOnboarding") {
				ContentView()
					.environment(\.managedObjectContext, persistenceController.container.viewContext)
					.environmentObject(sheetManager)
			}else{
				Onboarding()
					.environment(\.managedObjectContext, persistenceController.container.viewContext)
					.environmentObject(sheetManager)
			}
        }
    }
}
