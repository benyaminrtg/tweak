//
//  SavedBoardView.swift
//  Tweak
//
//  Created by Benyamin Rondang Tuahta on 02/12/20.
//

import SwiftUI
import CoreData

struct SavedBoardView: View {
    @FetchRequest(entity: Captions.entity(), sortDescriptors: []) var captionss: FetchedResults<Captions>
    @FetchRequest(entity: Boards.entity(), sortDescriptors: []) var boards: FetchedResults<Boards>
    @Environment(\.managedObjectContext) var moc
    @EnvironmentObject var partialSheetManager: PartialSheetManager
    @State var boardName: String
    @State var captionCount: Int
    @State var showPopover: Bool = false
    @State var isNewBoardModal: Bool = false
    @State var isAddToFolder: Bool = false
    @State var newCaption:[NSManagedObject] = []
    @State var showBlankState: Bool = true
    @Binding var selectedTab:Int
    @State var showPharaphase:Bool = false
    @State var captionShown = 0
    
    var body: some View {
        
        ZStack {
            Color("GrayBG")
                .edgesIgnoringSafeArea(.all)
            VStack {
                ScrollView{
                    if isAddToFolder != true {
                        ForEach(captionss, id: \.self) { caption in
                            
                            let board = caption.value(forKey: "board") as! [String]
                            if board.contains(boardName) {
                            
                                VStack(alignment: .leading){
                                    HStack{
                                        ImageViewWidget(imageUrl: caption.image ?? "unknown")
                                            .padding([.top, .leading])
                                        Text(caption.name ?? "unknown")
                                            .fontWeight(.semibold)
                                            .padding([.top])
                                        Spacer()
                                    }
                                    Text("\(caption.content ?? "unknown")")
                                        .padding([.leading, .trailing])
                                    Divider()
                                        .padding([.leading, .trailing])
                                    HStack{
                                        Spacer()
                                        Button(action: {
                                            //FIXME: paraphrase
                                            showPharaphase.toggle()
                                        }) {
                                            HStack{
                                                Spacer()
                                                Image(systemName: "square.and.pencil")
                                                    .foregroundColor(Color.black)
                                                Spacer()
                                            }
                                        }
                                        .padding(15)
                                        .fullScreenCover(isPresented: $showPharaphase, content: {
                                            ParaphraseView(profpic: caption.image ?? "", username: caption.name ?? "", caption: caption.content ?? "", showPharaphase: $showPharaphase, selectedTab: $selectedTab)
                                        })
                                        
                                        Spacer()
                                        Spacer()
                                        Button(action: {
                                            //FIXME: Bookmark
                                            if isBookmarked(id: Int(caption.captionId)) {
                                                self.deleteCaption(at: Int64(caption.captionId), boardName: boardName)
                                            }
                                            print("Bookmark")
                                        }) {
                                            HStack{
                                                Spacer()
                                                Image(systemName: "bookmark.fill")
                                                    .foregroundColor(Color("TweakOrange"))
                                                Spacer()
                                            }
                                        }
                                        .padding(15)
                                        //.foregroundColor(!isBookmarked() ? Color.black : Color.init(red: 234/245, green: 116/245, blue: 0/245))
                                        Spacer()
                                    }
                                }
                                .background(Color.white)
                                .cornerRadius(17)
                                .shadow(color: Color.black.opacity(0.1), radius: 10, x:2, y:2)
                                .padding([.top, .horizontal])
                                .onAppear{
                                    captionShown += 1
                                }
                                .onDisappear{
                                    captionShown -= 1
                                }
    //                            ExploreCard(profpicURL: caption.image, username: caption.name, captionBody: caption.content, id: caption.captionId, selectedTab: $selectedTab, boardName: $boardName)
    //                            Text("=====1=====\(caption.content ?? "asdf")")
    //                            Divider()
    //                            Text("======2=======\(caption.image ?? "asdf")")
    //                            Divider()
    //                            Text("==========3=========\(caption.name ?? "asdf")")
    //                            Divider()
    //                            Text("============4==========\(caption.captionId)")
    //                            let board = caption.value(forKey: "board") as! [String]
    //                            if board.contains(boardName) {
    //                                ExploreCard(profpicURL: caption.image, username: caption.name, captionBody: caption.content, id: caption.captionId,selectedTab:$selectedTab, boardName: $boardName)
    //                                    .background(Color.white)
    //                                    .cornerRadius(17)
    //                                    .shadow(color: Color.black.opacity(0.1), radius: 10, x:2, y:2)
    //                                    .padding([.top, .horizontal])
    //                            }
                            }
                        }
                    }
                }
            }
            
            if self.showPopover {
                HStack{
                    Spacer()
                    VStack {
                        CustomPopover(boardName: $boardName ,showPopover: $showPopover, isNewBoardModal: $isNewBoardModal, isAddToFolder: $isAddToFolder, showBlankState: $showBlankState)
                            .background(Color.white)
                            .clipShape(Rectangle())
                            .cornerRadius(15)
                            .shadow(color: Color.black.opacity(0.1), radius: 10, x:2, y:2)
                            .padding(.trailing,10)
                        Spacer()
                    }
                }
            }
            
            if self.captionShown == 0 {
                VStack(alignment: .center){
                    Image("TweakEmpty")
                    
                    Text("You don't have any saved captions yet")
                        .font(.callout)
                        .fontWeight(.semibold)
                        .foregroundColor(.secondary)
                        .padding(.top)
                    HStack{
                        Text("Tap on the")
                            .font(.callout)
                            .fontWeight(.regular)
                            .foregroundColor(.secondary)
                        Image(systemName: "folder.badge.plus")
                            .foregroundColor(.secondary)
                        Text("button to add a new board")
                            .font(.callout)
                            .fontWeight(.regular)
                            .foregroundColor(.secondary)
                    }
                    .padding(.top, 2)
                    
                }
            }
            
        }
        .navigationBarTitle(boardName, displayMode: .inline)
        .navigationBarItems(trailing:
                                Button(action: {
                                    if !isAddToFolder {
                                        withAnimation(.spring()) {
                                            self.showPopover.toggle()
                                        }
                                    } else {
//                                        loadCaption()
                                        var captionCounter = 0
                                        for caption in newCaption {
                                            let isChecked = caption.value(forKey: "isChecked") as! Bool
                                            if isChecked == true {
                                                captionCounter += 1
                                                var board = caption.value(forKey: "board") as! [String]
                                                board.append(boardName)
                                                caption.setValue(board, forKey: "board")
                                                caption.setValue(false, forKey: "isChecked")
                                            }
                                        }
                                        
                                        for item in boards {
                                            if item.value(forKey: "name") as! String == boardName {
                                                item.captionCount += Int16(captionCounter)
                                            }
                                        }
                                        
                                        try? moc.save()
                                        self.isAddToFolder.toggle()
                                    }
                                }) {
                                    if !isAddToFolder {
                                        Image(systemName: "ellipsis.circle")
                                            .resizable()
                                            .frame(width: 20, height: 20)
                                            .font(Font.body.weight(.regular))
                                    } else {
                                        Text("Done")
                                    }
                                }
        )
        .background((captionss.count == 0) ? Color.white : Color.init(red: 245/255, green: 245/255, blue: 245/255))
    }
    
//    func loadCaption() {
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
//        let managedContext = appDelegate.persistentContainer.viewContext
//        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Captions")
//        do {
//            newCaption = try managedContext.fetch(fetchRequest)
//        } catch let error as NSError {
//            print("Could not fetch. \(error), \(error.userInfo)")
//        }
//    }
    func isBookmarked(id: Int) -> Bool {
        for item in captionss {
            if item.value(forKey: "captionId") as! Int64 == id {
                _ = item
                return true
            }
        }
        return false
    }
    
    func deleteCaption(at id: Int64, boardName: String) {
        
        for item in captionss {
            if item.value(forKey: "captionId") as! Int64 == id {
                let deleteIndex = item
                moc.delete(deleteIndex)
            }
            
            if boardName == "" {
                let captionBoards = item.value(forKey: "board") as? [String] ?? ["Unknown"]
                for captionBoard in captionBoards {
                        for item2 in boards {
                            if item2.value(forKey: "name") as! String == captionBoard {
                                item2.captionCount -= 1
                            }
                        }
                }
            }
        }
        
        if boardName != "" {
            for item in boards {
                if item.value(forKey: "name") as! String == boardName {
                    item.captionCount -= 1
                }
            }
        }
        try? moc.save()
    }
    
//    func saveCaption(boardName: String) {
//        let image = self.profpicURL
//        let name = self.username
//        let content = self.captionBody
//        let bookmarked = true
//        let caption = Captions(context: self.moc)
//        caption.id = UUID()
//        caption.image = "\(image)"
//        caption.name = "\(name)"
//        caption.content = "\(content)"
//        caption.bookmarked = bookmarked
//        caption.board = ["\(boardName)"] as NSObject
//        caption.isChecked = false
//
//        self.toggle()
//
//        try? self.moc.save()
//    }
//
//    func saveCaptionAddBoardCount(at boardName: String){
//        for item in boards {
//            if item.value(forKey: "name") as! String == boardName {
//                item.captionCount += 1
//            }
//        }
//        try? moc.save()
//    }
    
    func saveCaptionToBoard(at content: String) {
        for caption in captionss {
            if caption.value(forKey: "content") as! String == content {
                caption.isChecked = !caption.isChecked
            }
        }
    }
}

//struct SavedBoardView_Previews: PreviewProvider {
//    static var previews: some View {
//        SavedBoardView()
//    }
//}
