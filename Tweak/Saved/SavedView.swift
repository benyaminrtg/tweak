//
//  SavedView.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI

struct SavedView: View {
    @State private var searchText:String = ""
    @State private var showAddFolder:Bool = false
    @State var boardName: String = ""
    @State var boardShown = 0
    @State var boardExist: Bool = false
    @Binding var selectedTab: Int
    @EnvironmentObject var partialSheetManager: PartialSheetManager
    @FetchRequest(entity: Boards.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \Boards.name, ascending: true)]) var boardList: FetchedResults<Boards>
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    var body: some View {
        NavigationView{
            ZStack{
                Color("GrayBG")
                    .edgesIgnoringSafeArea(.all)
                
                //MARK: Contents
                VStack(alignment: .leading){
                    Spacer()
                        .frame(height: 50)
                    //FIXME: Show Boards
                    
                    if boardList.count != 0 {
                        HStack {
                            ScrollView{
                                LazyVGrid(columns: columns, spacing: 12) {
                                    ForEach(boardList, id: \.self) { board in
                                        if(String(board.name ?? "").uppercased().contains(searchText.uppercased()) || searchText == ""){
                                            
                                            
                                            NavigationLink(destination: SavedBoardView(boardName: board.name ?? "unknown", captionCount: Int(board.captionCount), selectedTab: $selectedTab)) {
                                                VStack(alignment: .leading){
                                                    HStack{
                                                        Image(systemName: "folder.circle.fill")
                                                            .resizable()
                                                            .frame(width: 30, height: 30)
                                                            .padding([.leading, .top])
                                                            .foregroundColor(Color("TweakOrange"))
                                                        Spacer()
                                                        Text("\(board.captionCount)")
                                                            .font(.title)
                                                            .fontWeight(.semibold)
                                                            .padding([.trailing, .top])
                                                            .foregroundColor(Color.black)
                                                    }
                                                    Text("\(board.name ?? "Naming Error")")
                                                        .font(.subheadline)
                                                        .padding([.leading, .bottom])
                                                        .foregroundColor(Color.black)
                                                }
                                                .background(Color.white)
                                                .cornerRadius(17)
                                                .shadow(color: Color.black.opacity(0.1), radius: 10, x:2, y:2)
                                                .padding(.top)
                                                .padding(.horizontal, 10)
                                                .onAppear{
                                                    boardShown += 1
                                                }
                                                .onDisappear{
                                                    boardShown -= 1
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                            }
                        }
                        .padding(.horizontal, 5)
                    }else{
                        VStack(alignment: .center){
                            Image("TweakEmpty")
                            
                            Text("You don't have any saved captions yet")
                                .font(.callout)
                                .fontWeight(.semibold)
                                .foregroundColor(.secondary)
                                .padding(.top)
                            HStack{
                                Text("Tap on the")
                                    .font(.callout)
                                    .fontWeight(.regular)
                                    .foregroundColor(.secondary)
                                Image(systemName: "folder.badge.plus")
                                    .foregroundColor(.secondary)
                                Text("button to add a new board")
                                    .font(.callout)
                                    .fontWeight(.regular)
                                    .foregroundColor(.secondary)
                            }
                            .padding(.top, 2)
                            
                        }
                    }
                    
                }
                .alert(isPresented: $boardExist) {
                    Alert(title: Text("Fail to create board"), message: Text("Board has already exist!"), dismissButton: .default(Text("OK")))
                }
                
                //MARK: Searchbar
                VStack{
                    VStack(spacing:0){
                        HStack{
                            SearchBar(text: $searchText)
                            
                            ZStack(alignment: .topTrailing) {
                                Button(action: {
                                    self.partialSheetManager.showPartialSheet({
                                    }) {
                                        newBoard(partialSheetIsPresented: $partialSheetManager.isPresented, previousBoardName: $boardName, boardExist: $boardExist, isNewBoard: true, isFromExplore: false)
                                        //										ExploreAddBoardSheet(partialSheetIsPresented: $partialSheetManager.isPresented, previousBoardName: $previousBoardName , isNewBoard: true, isFromExplore: false)
                                    }
                                    //self.showAddFolder.toggle()
                                }) {
                                    Image(systemName: "folder.badge.plus")
                                        .resizable()
                                        .frame(width: 28, height: 20)
                                        .padding(.trailing, 4)
                                        .padding(.leading)
                                }
                                //.sheet(isPresented: $showAddFolder) {
                                //TODO: Filter View
                                /*
                                 NavigationView{
                                 ExploreFilterView(popBack: $showFilter)
                                 .environmentObject(userSettings)
                                 .environmentObject(captionList)
                                 }
                                 */
                                //}
                            }
                        }
                        .padding([.leading, .trailing, .top])
                        .padding(.bottom)
                        
                    }
                    .frame(height:50)
                    .background(Color("GrayBG"))
                    
                    Spacer()
                }
                
                if boardShown == 0 && boardList.count != 0 {
                    VStack{
                        Spacer()
                        HStack {
                            Spacer()
                            Image("TweakSearchNotFound")
                            Spacer()
                        }
                        HStack {
                            Text("Quack, board with title \"\(searchText)\" can't be found.")
                                .font(.callout)
                                .fontWeight(.semibold)
                                .foregroundColor(.gray)
                                .padding(.top)
                                .multilineTextAlignment(.center)
                        }
                        HStack {
                            Text("Try using different search terms.")
                                .font(.callout)
                                .foregroundColor(.gray)
                        }
                        .padding(.top, 2)
                        Spacer()
                    }
                }
                
            }
            .navigationBarTitle("Saved")
        }
    }
}
