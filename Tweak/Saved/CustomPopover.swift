import SwiftUI

struct CustomPopover: View {
    @FetchRequest(entity: Boards.entity(), sortDescriptors: []) var boards: FetchedResults<Boards>
    @FetchRequest(entity: Captions.entity(), sortDescriptors: []) var captionss: FetchedResults<Captions>
    @EnvironmentObject var partialSheetManager: PartialSheetManager
    @Environment(\.managedObjectContext) var moc
    @State var boardExist: Bool = false
    @Binding var boardName: String
    @Binding var showPopover: Bool
    @Binding var isNewBoardModal: Bool
    @Binding var isAddToFolder: Bool
    @Binding var showBlankState: Bool
    
    var body: some View {
        VStack(alignment: .leading, spacing: 18) {
            Button(action: {
                showPopover.toggle()
                isAddToFolder.toggle()
                showBlankState.toggle()
            }) {
                
                HStack(spacing: 15) {
                    Text("Add to Folder")
                    Spacer()
                    Image(systemName: "checkmark.circle")
                    
                }
            }
            Divider()
            Button(action: {
                showPopover.toggle()
                self.partialSheetManager.showPartialSheet({
                }) {
                    newBoard(partialSheetIsPresented: $partialSheetManager.isPresented, previousBoardName: $boardName, boardExist: $boardExist, isNewBoard: false, isFromExplore: false)
                }
            }) {
                HStack(spacing: 15) {
                    Text("Edit Folder")
                    Spacer()
                    Image(systemName: "folder.badge.plus")
                    
                }
            }
            Divider()
            Button(action: {
                showPopover.toggle()
                deleteBoard(boardName: boardName)
            }) {
                HStack(spacing: 15) {
                    Text("Delete Folder")
                    Spacer()
                    Image(systemName: "trash")
                }
                .foregroundColor(.red)
            }
            
        }
        .foregroundColor(.black)
        .frame(width: 180)
        .padding()
        .background(Color.white)
    }
    
    func deleteBoard(boardName: String) {
        for item in boards {
            if item.value(forKey: "name") as! String == boardName {
                let deleteIndex = item
                moc.delete(deleteIndex)
            }
        }
        
        for item in captionss {
                
            var board = item.value(forKey: "board") as! [String]
            board.removeAll{
                $0 == boardName
            }
            let newBoard = board as? NSObject
            item.board = newBoard
        }
        try? moc.save()
    }
}

