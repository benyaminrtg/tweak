//
//  SavedBoardCardView.swift
//  Tweak
//
//  Created by Benyamin Rondang Tuahta on 02/12/20.
//

import SwiftUI

struct SavedBoardCardView: View {
    var boardName: String
    var captionCount: Int
    var icon: String
    
    var body: some View {
        VStack(alignment: .leading){
            HStack{
                Image(systemName: icon)
                    .resizable()
                    .frame(width: 30, height: 30)
                    .padding([.leading, .top])
                    .foregroundColor(Color("TweakOrange"))
                Spacer()
                Text("\(captionCount)")
                    .font(.title)
                    .fontWeight(.semibold)
                    .padding([.trailing, .top])
                    .foregroundColor(Color.black)
            }
            Text("\(boardName)")
                .font(.subheadline)
                .padding([.leading, .bottom])
                .foregroundColor(Color.black)
        }
        .background(Color.white)
        .cornerRadius(17)
        .shadow(color: Color.black.opacity(0.1), radius: 10, x:2, y:2)
        .padding(.top)
        .padding(.horizontal,7)
    }
}

struct SavedBoardCardView_Previews: PreviewProvider {
    static var previews: some View {
        SavedBoardCardView(boardName: "", captionCount: 1, icon: "")
    }
}
