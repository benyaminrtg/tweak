//
//  SavedCaptionCardView.swift
//  Tweak
//
//  Created by Benyamin Rondang Tuahta on 02/12/20.
//

import SwiftUI

struct SavedCaptionCardView: View {
    var image: String
    var name: String
    var content: String
    var board: [String]
    var id: String
    
    @FetchRequest(entity: Captions.entity(), sortDescriptors: []) var captionss: FetchedResults<Captions>
    @Environment(\.managedObjectContext) var moc
    @State var isChecked: Bool
    func check() {
        for caption in captionss {
            if !isChecked && caption.id?.uuidString == id {
                updateCaption(at: caption.content ?? "")
            }
            else if isChecked && caption.id?.uuidString == id {
                updateCaption(at: caption.content ?? "")
            }
        }
    }
        
    var body: some View {
        VStack(alignment: .leading){
            HStack{
                ImageViewWidget(imageUrl: image)
                    .padding([.top, .leading])
                Text(name)
                    .padding([.top])
                Spacer()
                Button(action: {
                    isChecked = !isChecked
                    check()
                }){
                    HStack{
                        if isChecked {
                            Image(systemName: "checkmark.circle.fill")
                                .resizable()
                                .frame(width: 20, height: 20)
                                .foregroundColor(Color(#colorLiteral(red: 0.9860159755, green: 0.4201447368, blue: 0, alpha: 1)))
                        } else {
                            Image(systemName: "circle")
                                .resizable()
                                .frame(width: 20, height: 20)
                                .foregroundColor(Color(#colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)))
                        }
                        
                        
                    }
                }
                .padding([.top, .trailing])
                    
            }
            Text(content)
                .padding([.leading, .trailing, .bottom])
        }
    }
    
    func isBookmarked() -> Bool {
        for item in captionss {
            if item.value(forKey: "content") as! String == content {
                return true
            }
        }
        return false
    }
    
    func updateCaption(at content: String) {
        for caption in captionss {
            if caption.value(forKey: "content") as! String == content {
                caption.isChecked = !caption.isChecked
            }
        }
        try? moc.save()
    }
}
