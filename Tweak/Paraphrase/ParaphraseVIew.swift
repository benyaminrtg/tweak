//
//  ParaphraseVIew.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI
import SwiftUIPager

struct ParaphraseView: View {
	@Environment(\.presentationMode) var presentationMode
	@State var profpic: String
	@State var username: String
	@State var caption: String
	
	@ObservedObject var captionList : ParaphasedCaptions = ParaphasedCaptions()
	
	@State var page: Int = 0
	@State var localCaption: [String] = []
	@Binding var showPharaphase:Bool
	@State var showModify:Bool = false
	@Binding var selectedTab:Int
	func getTimestring() -> String{
		let formatter = DateFormatter()
		formatter.dateFormat = "dd.MM.yy - HH:mm"
		return formatter.string(from: Date())
	}
	@State var refresher = false
	var body: some View {
		ZStack {
			VStack {
				Spacer()
					.frame(height:54)
				Color("GrayBG")
					.edgesIgnoringSafeArea(.all)
			}
			VStack(spacing:0){
				ZStack{
					HStack{
						Spacer()
						Text("Tweak Caption")
							.fontWeight(.semibold)
							.font(.system(size: 17))
						Spacer()
					}
					.padding([.horizontal,.top,.bottom])
					.background(Color(red: 248, green: 248, blue: 248, opacity: 1))
					HStack{
						Button(action: {
							showPharaphase = false
							//self.presentationMode.wrappedValue.dismiss()
						}) {
							HStack {
								Image(systemName: "chevron.left")
									.font(Font.body.weight(.bold))
									.foregroundColor(Color("TweakOrange"))
								Text("Explore")
									.font(.body)
									.foregroundColor(Color("TweakOrange"))
									.font(.system(size: 17))
							}
							
						}
						Spacer()
					}.padding([.horizontal,.top,.bottom])
					
				}
				
				
				Divider()
				Spacer()
					.frame(height:10)
				ParaphaseInputCard(profpic: profpic, username: username, caption: caption)
				Text("Tweaked Caption")
					.bold()
					.padding(.top)
					.padding(.bottom,5)
				
				if refresher == false{
					ParaphaseOutputLoading()
						.offset(CGSize(width: 0, height: 10.0))
				}else{
					Pager(page: self.$page, data: localCaption, id: \.self, content: { item in
						ParaphaseOutput(pharaphasedCaption: item,username: username)
					})
					.rotation3D()
					.frame(height:225)
					.animation(.easeInOut(duration: 0.5))
					.transition(.opacity)
					.offset(CGSize(width: 0, height: -10.0))
					HStack{
						ForEach(localCaption,id: \.self){i in
							if localCaption[page] == i{
								Ellipse()
									.frame(width: 7, height: 7)
									.foregroundColor(Color.black)
									.onTapGesture {
										self.page = self.page
									}
							}else{
								Ellipse()
									.frame(width: 7, height: 7)
									.foregroundColor(Color.gray)
									.onTapGesture {
										self.page = localCaption.firstIndex(of: i) ?? 0
									}
							}
						}
					}
					.frame(height:10)
					.animation(.easeInOut(duration: 0.5))
					.transition(.opacity)
					.offset(CGSize(width: 0, height: -15.0))
				}
				Spacer()
				if refresher == false {
					Button(action: {}) {
						Text("Modify Caption!")
							.fontWeight(.semibold)
							.padding(.all)
							.padding(.horizontal,40)
					}
					.disabled(true)
					.background(Color.gray)
					.cornerRadius(25)
					.foregroundColor(.white)
				}else{
					Button(action: {
						print("modify")
						showModify.toggle()
					}) {
						Text("Modify Caption!")
							.fontWeight(.semibold)
							.padding(.all)
							.padding(.horizontal,40)
							.foregroundColor(.white)
					}
					.background(Color("TweakOrange"))
					.cornerRadius(25)
					.fullScreenCover(isPresented: $showModify, content: {
						ModifyCaptionView(caption: localCaption[self.page], title: "", showPharaphase: $showPharaphase, showModify: $showModify,selectedTab:$selectedTab)
					})
					/*NavigationLink(
						destination: ModifyCaptionView(caption: localCaption[self.page], title: "\(username) \(getTimestring())"),
						label: {
							Text("Modify Caption!")
								.fontWeight(.semibold)
								.padding(.all)
								.padding(.horizontal,40)
								.background(Color("TweakOrange"))
								.cornerRadius(25)
								.foregroundColor(.white)
						}
					)*/
				}
			}
			.navigationBarTitle("Tweak Caption", displayMode: .inline)
			.navigationBarBackButtonHidden(true)
			.navigationBarItems(leading:
									Button(action: {
										self.presentationMode.wrappedValue.dismiss()
										refresher = false
									}) {
										HStack(spacing: 0){
											Image(systemName: "chevron.left")
												.foregroundColor(Color("TweakOrange"))
											Text(" Explore")
												.foregroundColor(Color("TweakOrange"))
												.fontWeight(.regular)
											Spacer()
										}
									})
			.padding(.vertical)
			.onAppear{
				refreseher()
				self.captionList.paraphaseStart(inputCaption: caption, qty: 5)
			}
		}
	}
	
	func refreseher(){
		if self.refresher == false{
			DispatchQueue.main.async {
				localCaption = self.captionList.paraphasedCaptions
				if self.captionList.ready && self.captionList.paraphasedCaptions.count > 0{
					self.refresher = self.captionList.ready
					refreseher()
				}else{
					refreseher()
				}
				
			}
		}
		
	}
}
