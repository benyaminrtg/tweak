import SwiftUI
struct ParaphaseOutput: View {
	@State var pharaphasedCaption:String
	@State var username:String
	var body: some View {
		HStack{
			VStack(alignment: .leading){
				ScrollView{
					HStack{
						Text(pharaphasedCaption)
							.font(.system(size: 17))
						Spacer()
					}
				}
				.padding()
				.cornerRadius(15.0)
				.frame(height: 150)
				
				HStack(spacing:0){
					Text("Tweaked from ")
						.font(.system(size: 12))
						.foregroundColor(.gray)
					
					Text(username)
						.font(.system(size: 12))
						.foregroundColor(.gray)
						.bold()
					Spacer()
				}.padding([.bottom,.horizontal])
			}
			.background(Color.white)
			.cornerRadius(15.0)
			.frame(height: 180)
			.shadow(color: Color.black.opacity(0.1),radius: 10, x:2, y:2)
		}
		.padding(.horizontal)
	}
}
