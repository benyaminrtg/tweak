import SwiftUI

struct ParaphaseInputCard: View {
	@State var profpic: String
	@State var username: String
	@State var caption: String
	var body: some View {
		HStack{
			VStack(alignment: .leading){
				HStack{
					ImageViewWidget(imageUrl: profpic)
					Text(username)
						.bold()
					Spacer()
				}
				ScrollView{
					HStack{
						Text(caption)
					}
				}.frame(height: 159)
			}
			.padding()
			.background(Color.white)
			.cornerRadius(15.0)
			.shadow(color: Color.black.opacity(0.1),radius: 10, x:2, y:2)
		}.padding(.horizontal)
	}
}
