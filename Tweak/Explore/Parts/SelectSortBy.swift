//
//  SelectSortBy.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI

struct SelectSortBy: View {
	@Environment(\.presentationMode) var presentationMode
	@Binding var sortBy:String
	@State var sortCategory:[String] = ["Relevancy","Most Engaging","Top Liked","Top Commented","Most Popular","Hot Topic","Most Recent"]
	
	var body: some View {
		ZStack{
			Color("GrayBG")
				.edgesIgnoringSafeArea(.all)
			
			VStack {
				ZStack{
					HStack{
						Spacer()
						Text("Filter & Sort")
							.fontWeight(.semibold)
						Spacer()
					}
					.padding([.horizontal,.top,.bottom])
					.background(Color(red: 248, green: 248, blue: 248, opacity: 1))
					HStack{
						Button(action: {
							self.presentationMode.wrappedValue.dismiss()
						}) {
							Text("Cancel")
								.font(.body)
								.foregroundColor(Color("TweakOrange"))
						}
						Spacer()
					}.padding([.horizontal,.top,.bottom])
					
				}
				
				
				Divider()
					.offset(y:-8)
				
				VStack{
					
					VStack{
						ForEach(sortCategory,id: \.self){item in
							Button(action: {
								
								sortBy = item
								self.presentationMode.wrappedValue.dismiss()
							}) {
								HStack {
									Text(item.capitalized)
										.foregroundColor(.black)
										.padding(.bottom,5)
									Spacer()
								}
							}
							if item != sortCategory.last {
								Divider()
							}
						}
					}
					.padding()
					.background(Color.white)
					.cornerRadius(15)
					Spacer()
				}
				.padding()
			}
		}
	}
}

struct SelectSortBy_Previews: PreviewProvider {
    static var previews: some View {
		SelectSortBy(sortBy: .constant("Lorem"))
    }
}
