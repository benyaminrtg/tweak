//
//  SelectCategoryView.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI

struct SelectCategoryView: View {
	@Environment(\.presentationMode) var presentationMode
	@Binding var businessCategory:String
	@State var listCategory:[String] = []
	
	var body: some View {
		ZStack{
			Color("GrayBG")
				.edgesIgnoringSafeArea(.all)
				
			
			VStack {
				ZStack{
					HStack{
						Spacer()
						Text("Category")
							.fontWeight(.semibold)
						Spacer()
					}
					.padding([.horizontal,.top,.bottom])
					.background(Color(red: 248, green: 248, blue: 248, opacity: 1))
					HStack{
						Button(action: {
							self.presentationMode.wrappedValue.dismiss()
						}) {
							Text("Cancel")
								.font(.body)
								.foregroundColor(Color("TweakOrange"))
						}
						Spacer()
					}.padding([.horizontal,.top,.bottom])
					
				}
				
				
				Divider()
					.offset(y:-8)
				
				VStack{
					
					VStack{
						ForEach(listCategory,id: \.self){item in
							Button(action: {
								
								businessCategory = item
								self.presentationMode.wrappedValue.dismiss()
							}) {
								HStack {
									Text(item.capitalized == "Uncategorized" ? "All" : item.capitalized)
										.foregroundColor(.black)
										.padding(.bottom,5)
									Spacer()
								}
							}
							if item != listCategory.last {
								Divider()
							}
						}
					}
					.padding()
					.background(Color.white)
					.cornerRadius(15)
					.onAppear{loadCategory()}
					Spacer()
				}
				.padding()
			}
		}
	}
	func loadCategory() {
		self.listCategory.removeAll()
		guard let url = URL(string: "https://tweak.omkstyakobus.org/categorylist") else {
			print("Invalid URL")
			return
		}
		print(url)
		let request = URLRequest(url: url)
		URLSession.shared.dataTask(with: request) { data, response, error in
			if let data = data {
				
				let responseData = String(data: data, encoding: String.Encoding.utf8)!.replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: "").replacingOccurrences(of: "\"", with: "").components(separatedBy: ",")
				listCategory = responseData
				print(listCategory)
				
			}
		}.resume()
	}
}

struct SelectCategoryView_Previews: PreviewProvider {
    static var previews: some View {
		SelectCategoryView(businessCategory: .constant("Lorem"))
    }
}
