//
//  ExploreCard.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI
import PartialSheet

struct ExploreCard: View {
    @FetchRequest(entity: Boards.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \Boards.name, ascending: true)]) var boards: FetchedResults<Boards>
    @FetchRequest(entity: Captions.entity(), sortDescriptors: []) var captions: FetchedResults<Captions>
	var profpicURL:String
	var username:String
	var captionBody:String
	var id:Int
	@State var reportReason:String = ""
	@State var showActionSheet: Bool = false
	@EnvironmentObject var partialSheetManager: PartialSheetManager
	@Environment(\.managedObjectContext) var moc
	@Binding var selectedTab:Int
	@State var showPharaphase:Bool = false
    @State var isChecked:Bool = false
    @State var boardExist: Bool = false
    func toggle(){isChecked = !isChecked}
    @Binding var boardName: String
    var body: some View {
		VStack(alignment: .leading){
			HStack{
				ImageViewWidget(imageUrl: profpicURL)
					.padding([.top, .leading])
				Text(username)
					.fontWeight(.semibold)
					.padding([.top])
				Spacer()
				Image(systemName: "ellipsis")
					.padding([.top, .trailing])
					.onTapGesture {
						self.showActionSheet = true
					}
					.actionSheet(isPresented: $showActionSheet) {
						ActionSheet(title: Text("Report inappropriate contents. Block accounts that you don't want to see."), buttons: [
							.destructive(Text("Report")) {
								print("Report")
								self.partialSheetManager.showPartialSheet({
								}) {
									VStack{
										TextField("Report reason", text: $reportReason)
											.padding()
										Button("Send Report", action: {
											reportCaption()
											self.partialSheetManager.closePartialSheet()
										})
										.padding()
									}
								}
							},
							.default(Text("Block \(self.username)")) {
								print("Block")
								blockAccount(account: self.username)
							},
							.cancel()
						])
					}
			}
			Text(captionBody)
				.padding([.leading, .trailing])
			Divider()
				.padding([.leading, .trailing])
			HStack{
				Spacer()
				Button(action: {
					showPharaphase.toggle()
				}) {
					HStack{
						Spacer()
						Image(systemName: "square.and.pencil")
							.foregroundColor(Color.black)
						Spacer()
					}
				}
				.padding(15)
				.fullScreenCover(isPresented: $showPharaphase, content: {
					ParaphraseView(profpic: profpicURL, username: username, caption: captionBody,showPharaphase:$showPharaphase,selectedTab:$selectedTab)
				})
				
				Spacer()
				Spacer()
				Button(action: {
					//FIXME: Bookmark
                    if isBookmarked() {
                        self.deleteCaption(at: Int64(id), boardName: boardName)
                    } else {
                        self.partialSheetManager.showPartialSheet({
                        }) {
                            VStack {
                                HStack{
                                    Image(systemName: "plus")
                                        .resizable()
                                        .frame(width: 20, height: 20)
                                        .foregroundColor(.clear)
                                        .padding(.leading)
                                    Spacer()
                                    Text("Save to")
                                        .fontWeight(.semibold)
                                    Spacer()
                                    Button(action: {
                                        self.partialSheetManager.showPartialSheet({
                                        }) {
                                            newBoard(partialSheetIsPresented: $partialSheetManager.isPresented, previousBoardName: $boardName, boardExist: $boardExist, isNewBoard: true, isFromExplore: true)
                                        }
                                    }) {
                                        Image(systemName: "plus")
                                            .resizable()
                                            .frame(width: 20, height: 20)
                                            .padding(.trailing)
                                    }
                                }
                                .padding(.bottom)
                                
                                ZStack {
                                    Color("GrayBG")
                                        .edgesIgnoringSafeArea(.all)
                                    
                                    if boards.count == 0 {
                                        HStack{
                                            Text("Tap on the")
                                                .font(.callout)
                                                .fontWeight(.regular)
                                                .foregroundColor(.secondary)
                                            Image(systemName: "plus")
                                                .foregroundColor(.secondary)
                                            Text("button to add a new board")
                                                .font(.callout)
                                                .fontWeight(.regular)
                                                .foregroundColor(.secondary)
                                        }.padding(.vertical, 58)
                                    } else {
                                        ScrollView(.horizontal, showsIndicators: false) {
                                            HStack {
                                                ForEach(boards, id:\.self) { board in
                                                    Button(action: {
                                                        if !isBookmarked() {
                                                            self.saveCaption(boardName: board.name ?? "Unknown")
                                                            self.saveCaptionAddBoardCount(at: board.name ?? "Unknown")
                                                        }
                                                        self.partialSheetManager.isPresented = false
                                                    }) {
                                                        SavedBoardCardView(boardName: board.name ?? "Unknown", captionCount: Int(board.captionCount), icon: board.icon ?? "folder.circle.fill")
                                                            .frame(width: 170)
                                                            .padding(.bottom)
                                                            .padding(.leading, 10)
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }.frame(height: 95)
                            }
                        }
                    }
					print("Bookmark")
				}) {
					HStack{
						Spacer()
                        Image(systemName: isBookmarked() ? "bookmark.fill" : "bookmark")
							.foregroundColor(!isBookmarked() ? Color.black : Color.init(red: 234/245, green: 116/245, blue: 0/245))
						Spacer()
					}
				}
				.padding(15)
				//.foregroundColor(!isBookmarked() ? Color.black : Color.init(red: 234/245, green: 116/245, blue: 0/245))
				Spacer()
			}
		}
		
    }
	func reportCaption() {
		let originalString = reportReason
		let reason = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
		let id = self.id
		if let url = URL(string: "https://tweak.omkstyakobus.org/report/\(reason ?? "error")/\(id)") {
			URLSession.shared.dataTask(with: url) { data, response, error in
				if let data = data {
					DispatchQueue.main.async {
						if String(decoding: data, as: UTF8.self) == "1" {
							let alert = UIAlertController(title: "Report", message: "Report Success", preferredStyle: .alert)
							alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
						}else{
							let alert = UIAlertController(title: "Report", message: "Report Failed", preferredStyle: .alert)
							alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
						}
					}
				}
			}.resume()
		}
	}
	
	func blockAccount(account: String) {
		print("---\(account)")
		let blockedAccounts = BlockedAccounts(context: moc)
		blockedAccounts.username = account
		try? moc.save()
	}
    
    func isBookmarked() -> Bool {
        for item in captions {
            if item.value(forKey: "captionId") as! Int64 == id {
                _ = item
                return true
            }
        }
        return false
    }
    
    func deleteCaption(at id: Int64, boardName: String) {
        self.toggle()
        
        for item in captions {
            if item.value(forKey: "captionId") as! Int64 == id {
                let deleteIndex = item
                moc.delete(deleteIndex)
            }
            
            if boardName == "" {
                let captionBoards = item.value(forKey: "board") as? [String] ?? ["Unknown"]
                for captionBoard in captionBoards {
                        for item2 in boards {
                            if item2.value(forKey: "name") as! String == captionBoard {
                                item2.captionCount -= 1
                            }
                        }
                }
            }
        }
        
        if boardName != "" {
            for item in boards {
                if item.value(forKey: "name") as! String == boardName {
                    item.captionCount -= 1
                }
            }
        }
        try? moc.save()
    }
    
    func saveCaption(boardName: String) {
        var alreadyExist = false
        for caption in captions {
            print(caption.captionId)
            print(self.id)
            if caption.captionId == Int64(self.id) {
                print("udah ada")
                var oldBoard = caption.board as! [String]
                oldBoard.append("\(boardName)")
                print(oldBoard)
                try? self.moc.save()
                alreadyExist = true
            }
        }
        
        if !alreadyExist {
            print("create new")
            let image = self.profpicURL
            let name = self.username
            let content = self.captionBody
            let bookmarked = true
            let caption = Captions(context: self.moc)
            caption.id = UUID()
            caption.image = "\(image)"
            caption.name = "\(name)"
            caption.content = "\(content)"
            caption.bookmarked = bookmarked
            caption.board = ["\(boardName)"] as NSObject
            caption.isChecked = false
            caption.captionId = Int64(self.id)
            
            self.toggle()
            
            try? self.moc.save()
        }
    }
    
    func saveCaptionAddBoardCount(at boardName: String){
        for item in boards {
            if item.name == boardName {
                item.captionCount += 1
            }
        }
        try? moc.save()
    }
}

//struct ExploreCard_Previews: PreviewProvider {
//    static var previews: some View {
//		ExploreCard(profpicURL: "a", username: "Lorem", captionBody: "Lorem", id: 1, selectedTab: .constant(1))
//    }
//}

