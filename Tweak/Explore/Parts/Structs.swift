//
//  CaptionFetcher.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI
import Combine

struct ServerCaptionDataResponse: Codable {
	var serverCaptionData: [ServerCaptionData]
}

struct ServerCaptionData: Codable {
	var id: Int
	var businessCategory: String
	var caption: String
	var username: String
	var profpicURL: String
	var archetype: String
	var objective: String
	var picURL: String
}

struct ObjectiveList: Codable {
	var objective: String
}

struct ActivityIndicator: UIViewRepresentable {
	let style: UIActivityIndicatorView.Style
	
	func makeUIView(context: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
		return UIActivityIndicatorView(style: style)
	}
	
	func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicator>) {
		uiView.startAnimating()
	}
}


struct ImageViewWidget: View {
	@ObservedObject var imageLoader: ImageLoader
	
	@State var image:UIImage = UIImage()
	
	init(imageUrl: String) {
		imageLoader = ImageLoader(imageUrl: imageUrl)
	}
	
	var body: some View {
		
		if let image = UIImage(data: imageLoader.data) {
			return AnyView(
				Image(uiImage: image).resizable()
					.clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
					.frame(width: 20.0, height: 20.0)
			)
		}
		else {
			return AnyView(
				ActivityIndicator(style: .medium)
			)
		}
	}
}

struct BookmarkButton: View {
	@Binding var isOn: Bool
	func toggle(){isOn = !isOn}
	var body: some View {
		
		
		
		Button(action: toggle) {
			Image(isOn ? "bookmark.fill" : "bookmark")
		}
	}
}
