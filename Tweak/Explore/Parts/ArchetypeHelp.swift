//
//  ArchetypeHelp.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI

struct ArchetypeHelp: View {
	@Environment(\.presentationMode) var presentationMode
	@Binding var showArchetypesHelp:Bool
	var archetypes = ["Innocent", "Sage", "Explorer", "Outlaw", "Magician", "Hero", "Lover", "Jester", "Everyman", "Caregiver", "Ruler", "Creator"]
	var archetypesDescription = ["This personality promises safety and simplicity. The characteristics are optimist, honest and humble.", "This personality promises understanding and wisdom. The characteristics are knowledgable, assured and guiding.", "This personality promises freedom. The characteristics are exciting, fearless and daring.", "This personality promises revolution. The characteristics are disruptive, rebellious and combative.", "This personality promises power and knowledge. The characteristics are mystical, informed and reassuring.", "This personality promises mastery and triumph. The characteristics are honest, candid and brave.", "This personality promises intimacy and passion. The characteristics are sensual, empatheric and soothing.", "This personality promises pleasure and entertainment. The characteristics are fun, playful and optimistic.", "This personality promises belonging. The characteristics are friendly, humble and authentic.", "This personality promises service and recognition. The characteristics are caring, warm and reassuring.", "This personality promises control and power. The characteristics are commanding, refined and articulate.", "This personality promises innovation and authenticity. The characteristics are inspirational, daring and provocative."]
	var body: some View {
		ZStack{
			Color("GrayBG")
				.edgesIgnoringSafeArea(.all)
			
			
			VStack {
				ZStack{
					HStack{
						Spacer()
						Text("What is Brand Personality?")
							.fontWeight(.semibold)
						Spacer()
					}
					.padding([.horizontal,.top,.bottom])
					.background(Color(red: 248, green: 248, blue: 248, opacity: 1))
					HStack{
						Spacer()
						Button(action: {
							self.presentationMode.wrappedValue.dismiss()
						}) {
							Text("Done")
								.fontWeight(.semibold)
						}
					}.padding([.horizontal,.top,.bottom])
				}
				
				Divider()
					.offset(y:-8)
				
				VStack{
					Text("Tweak’s objective filtering function enables you to easily find marketing campaign inspirations for your business. Below are a brief overview of what each marketing campaign does & how your brand can benefit from it.")
						.font(.footnote)
						.foregroundColor(.secondary)
						.padding(.bottom)
					ScrollView(showsIndicators: true) {
						VStack(alignment: .leading, spacing:0){
							ForEach(0 ..< archetypes.count) {
								Text(archetypes[$0])
									.font(.callout)
									.padding(.bottom, 4)
								Text(archetypesDescription[$0])
									.font(.footnote)
									.foregroundColor(.secondary)
									.padding(.bottom, 24)
							}
						}
					}
				}
				.padding(.horizontal)
			}
		}
	}
}

struct ArchetypeHelp_Previews: PreviewProvider {
    static var previews: some View {
		ArchetypeHelp(showArchetypesHelp: .constant(true))
    }
}
