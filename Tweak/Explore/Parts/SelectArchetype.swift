//
//  SelectArchetype.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI

struct SelectArchetype: View {
	@Environment(\.presentationMode) var presentationMode
	@Binding var archetype:String
	@State var listArchetype:[String] = []
	@State var showArchetypesHelp:Bool = false
	
	var body: some View {
		ZStack{
			Color("GrayBG")
				.edgesIgnoringSafeArea(.all)
			
			
			VStack {
				ZStack{
					HStack{
						Spacer()
						Text("Brand Personality")
							.fontWeight(.semibold)
						Spacer()
					}
					.padding([.horizontal,.top,.bottom])
					.background(Color(red: 248, green: 248, blue: 248, opacity: 1))
					HStack{
						Button(action: {
							self.presentationMode.wrappedValue.dismiss()
						}) {
							Text("Cancel")
								.font(.body)
								.foregroundColor(Color("TweakOrange"))
						}
						Spacer()
						Button(action: {
							self.showArchetypesHelp.toggle()
						}) {
							Image(systemName: "questionmark.circle")
								.padding(3)
						}
					}.padding([.horizontal,.top,.bottom])
					.sheet(isPresented: $showArchetypesHelp) {
						ArchetypeHelp(showArchetypesHelp: $showArchetypesHelp)
					}
					
				}
				
				
				Divider()
					.offset(y:-8)
				
				VStack{
					
					VStack{
						ForEach(listArchetype,id: \.self){item in
							Button(action: {
								archetype = item
								self.presentationMode.wrappedValue.dismiss()
							}) {
								HStack {
									Text(item.capitalized == "None" ? "All" : item.capitalized )
										.foregroundColor(.black)
										.padding(.bottom,5)
									Spacer()
								}
							}
							if item != listArchetype.last {
								Divider()
							}
						}
					}
					.padding()
					.background(Color.white)
					.cornerRadius(15)
					.onAppear{loadArchetype()}
					Spacer()
				}
				.padding()
			}
		}
	}
	func loadArchetype() {
		self.listArchetype.removeAll()
		guard let url = URL(string: "https://tweak.omkstyakobus.org/archetypelist") else {
			print("Invalid URL")
			return
		}
		print(url)
		let request = URLRequest(url: url)
		URLSession.shared.dataTask(with: request) { data, response, error in
			if let data = data {
				
				let responseData = String(data: data, encoding: String.Encoding.utf8)!.replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: "").replacingOccurrences(of: "\"", with: "").components(separatedBy: ",")
				listArchetype = responseData
				print(listArchetype)
				
			}
		}.resume()
	}
}

struct SelectArchetype_Previews: PreviewProvider {
    static var previews: some View {
		SelectArchetype(archetype: .constant("Lorem"))
    }
}
