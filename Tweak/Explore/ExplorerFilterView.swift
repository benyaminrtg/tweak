//
//  ExplorerFilterView.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI

struct ExplorerFilterView: View {
	@Environment(\.presentationMode) var presentationMode
	@Binding var businessCategory:String
	@Binding var archetype:String
	@Binding var objective:String
	@Binding var sorter:String
	@State var newBusinessCategory:String = ""
	@State var newArchetype:String = ""
	@State var newObjective:String = ""
	@State var newSorter:String = ""
	
	@State private var showCategory:Bool = false
	@State private var showObjective:Bool = false
	@State private var showArchetype:Bool = false
	@State private var showSortBy:Bool = false
	@Binding var applyFilter:Bool
	var body: some View {
		ZStack{
			Color("GrayBG")
				.edgesIgnoringSafeArea(.all)
			
			VStack {
				ZStack{
					HStack{
						Spacer()
						Text("Filter & Sort")
							.fontWeight(.semibold)
						Spacer()
					}
					.padding([.horizontal,.top,.bottom])
					.background(Color(red: 248, green: 248, blue: 248, opacity: 1))
					HStack{
						Button(action: {
							self.presentationMode.wrappedValue.dismiss()
						}) {
							Text("Cancel")
								.font(.body)
								.foregroundColor(Color("TweakOrange"))
						}
						
						Spacer()
							
						Button(action: {
							if newBusinessCategory != "" {
								businessCategory = newBusinessCategory
								UserDefaults.standard.set(newBusinessCategory, forKey: "businessCategory")
							}
							if newArchetype != "" {
								archetype = newArchetype
							}
							if newObjective != "" {
								objective = newObjective
							}
							if newSorter != "" {
								sorter = newSorter
							}
							applyFilter.toggle()
							self.presentationMode.wrappedValue.dismiss()
						}) {
							Text("Apply")
								.fontWeight(.semibold)
								.foregroundColor(Color("TweakOrange"))
						}
						
					}.padding([.horizontal,.top,.bottom])
					
				}
				
				
				Divider()
					.offset(y:-8)
				
				VStack{
					HStack{
						Text("FILTER")
							.font(.system(size: 13))
							.foregroundColor(.gray)
						Spacer()
					}
					
					VStack{
						Button(action: {
							self.showCategory.toggle()
						}) {
							HStack {
								Text("Category")
									.foregroundColor(.black)
									.padding(.bottom,5)
								Spacer()
								Text(newBusinessCategory == "" ? businessCategory == "" ? "None" : businessCategory : newBusinessCategory)
									.foregroundColor(.gray)
									.padding(.bottom,5)
								Image(systemName:"chevron.right")
									.foregroundColor(.gray)
									.padding(.bottom,5)
							}
						}
						.sheet(isPresented: $showCategory) {
							SelectCategoryView(businessCategory: $newBusinessCategory)
						}
						Divider()
						
						Button(action: {
							self.showObjective.toggle()
						}) {
							HStack {
								Text("Objective")
									.foregroundColor(.black)
									.padding(.vertical,5)
								Spacer()
								Text(newObjective == "" ? objective == "" ? "None" : objective : newObjective)
									.foregroundColor(.gray)
									.padding(.bottom,5)
								Image(systemName:"chevron.right")
									.foregroundColor(.gray)
									.padding(.bottom,5)
							}
						}
						.sheet(isPresented: $showObjective) {
							SelectObjectiveView(objective: $newObjective)
						}
						Divider()
						
						Button(action: {
							self.showArchetype.toggle()
						}) {
							HStack {
								Text("Brand Personality")
									.foregroundColor(.black)
									.padding(.top,5)
								Spacer()
								Text(newArchetype == "" ? archetype == "" ? "None" : archetype : newArchetype)
									.foregroundColor(.gray)
									.padding(.bottom,5)
								Image(systemName:"chevron.right")
									.foregroundColor(.gray)
									.padding(.bottom,5)
							}
						}
						.sheet(isPresented: $showArchetype) {
							SelectArchetype(archetype: $newArchetype)
						}
					}
					.padding()
					.background(Color.white)
					.cornerRadius(15)
					HStack{
						Text("SORT")
							.font(.system(size: 13))
							.foregroundColor(.gray)
						Spacer()
					}.padding(.top)
					
					VStack{
						Button(action: {
							self.showSortBy.toggle()
						}) {
							HStack {
								Text("Sort by")
									.foregroundColor(.black)
									.padding(.bottom,5)
								Spacer()
								Text(newSorter == "" ? sorter == "" ? "Relevancy" : sorter : newSorter)
									.foregroundColor(.gray)
									.padding(.bottom,5)
								Image(systemName:"chevron.right")
									.foregroundColor(.gray)
									.padding(.bottom,5)
							}
						}
						.sheet(isPresented: $showSortBy) {
							SelectSortBy(sortBy: $newSorter)
						}
					}
					.padding()
					.background(Color.white)
					.cornerRadius(15)
					Spacer()
					Spacer()
				}
				.padding()
			}
		}
	}
}

struct ExplorerFilterView_Previews: PreviewProvider {
	static var previews: some View {
		ExplorerFilterView(businessCategory: .constant(String("Lorem")), archetype: .constant(String("Lorem")), objective: .constant(String("Lorem")), sorter: .constant(String("Lorem")), applyFilter: .constant(false))
	}
}
